# -*- coding: utf-8 -*-
"""
Created on Wed Mar 17 11:48:49 2021

@author: mussa
"""

from bs4 import BeautifulSoup
import requests
import json

#This Class is to scrap data from a given url

class WebScrapping:
    
    def __init__(self, url):
        self.url = url
    
    def data(self):
    
        complete = requests.get(self.url)
        soup = BeautifulSoup(complete.text, "html.parser")
        
        #Getting the 3 diff tables needed from the website
        #using the helper method get_table
        table = soup.find('table', id = "main_table_countries_today")
        df1 = self.get_table(table)
        
        table2 = soup.find('table', id = "main_table_countries_yesterday")
        df2= self.get_table(table2)
        
        table3 = soup.find('table', id = "main_table_countries_yesterday2")
        df3= self.get_table(table3)
        
        #appending the 3 tables in one array to then put it in the json file
        df = []
        df.append(df1)
        df.append(df2)
        df.append(df3)
        
        with open("ext/country_neighbour_dist_file.json", "w") as data:
                  json.dump(df, data)


    #this is a helper method that will modifiy the data before putting in
    #this database. Removed the coma's, the + etc to prepare the data for the database
    #and for the plots
    def modify_row(self,row):
        final_row = []
        for x in row:
            x = x.strip()
            x = x.replace(",", "")
            x = x.replace("+", "")
            x = None if x == "" else x
            x = None if x == "N/A" else x
            x = None if x == "\n" else x
            final_row.append(x)
        return final_row

    #this is also a helper method to be able to retrive the data
    #from the table, get the rows and remove the rows that are not wanted
    def get_table(self,table):
        table_rows = table.find_all('tr')
        output_rows = []
        for tr in table_rows[8:230]:
            td = tr.find_all('td')
            row = [i.text for i in td]
            row_modified = self.modify_row(row)
            output_rows.append(row_modified)
        return output_rows

