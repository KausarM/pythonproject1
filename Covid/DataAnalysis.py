import pandas as pd

from DBConnection import DBConnection
from Colors import colors


# Analyses the data from the database with pandas
class DataAnalysis:
    def __init__(self, db):
        self.db = db

    # Get the data from the database
    def fetch_data(self, country):
        sql = """SELECT total_cases, total_deaths, total_recovered, active_cases, critical, total_tests FROM today WHERE country = %s
                 UNION ALL SELECT total_cases, total_deaths, total_recovered, active_cases, critical, total_tests FROM yesterday WHERE country = %s
                 UNION ALL SELECT total_cases, total_deaths, total_recovered, active_cases, critical, total_tests FROM day_before WHERE country = %s"""
        self.db.cursor.execute(sql, (country, country, country))
        res = self.db.cursor.fetchall()
        data = pd.DataFrame(data=res, index=["Today", "Yesterday", "2 days ago"],
                            columns=["Total Cases", "Total Deaths",
                                     "Total Recovered",
                                     "Active Cases",
                                     "Critical", "Total Tests"])
        data.name = country

        return data

    # Compare all 3 days for a single country
    def compare_to_itself(self, data):
        print(f"{colors.HEADER}\nAnalyzing {data.name}...")
        print(f"{colors.OKBLUE}\nDifference: \n{colors.ENDC}", data.diff())
        print(f"{colors.OKBLUE}\nPercentage of difference: \n{colors.ENDC}", data.pct_change())

    # Compare a given country against others
    def compare_to_others(self, initial_data, *data):
        print(f"{colors.HEADER}\nComparing {len(data) + 1} countries...")

        for dataset in data:
            print(f"{colors.OKBLUE}\nComparing {initial_data.name} to {dataset.name}\n{colors.ENDC}")
            print(initial_data.compare(dataset))

    # Ask the user for countries and analyzes the data
    def analyze(self):
        first_country_data = self.fetch_data(input("Enter a country: "))
        # first_country_data = self.fetch_data("Canada")
        self.compare_to_itself(first_country_data)

        second_country_data = self.fetch_data(input("Enter a second country: "))
        third_country_data = self.fetch_data(input("Enter a third country: "))
        fourth_country_data = self.fetch_data(input("Enter a fourth country: "))
        fifth_country_data = self.fetch_data(input("Enter a fifth country: "))
        # second_country_data = self.fetch_data("Japan")
        # third_country_data = self.fetch_data("Uruguay")
        # fourth_country_data = self.fetch_data("France")
        # fifth_country_data = self.fetch_data("Germany")
        self.compare_to_others(first_country_data, second_country_data, third_country_data, fourth_country_data,
                               fifth_country_data)
