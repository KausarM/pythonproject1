# Etienne Plante - 1934877

import json
from DBConnection import DBConnection as db

# Creates tables and pushes data from json to them
class DBArchiver:
    def __init__(self, db):
        self.db = db

    # Returns a custom query to create a set table with the given name
    def get_create_table_query(self, name):
        sql = """CREATE TABLE {}(
                id INT,
                country VARCHAR(255),
                total_cases INT,
                new_cases INT,
                total_deaths INT,
                new_deaths INT,
                total_recovered INT,
                new_recovered INT,
                active_cases INT,
                critical INT,
                cases_per_million INT,
                deaths_per_million INT,
                total_tests INT,
                tests_per_million INT,
                population INT,
                continent VARCHAR(255),
                population_per_case INT,
                population_per_death INT,
                population_per_test INT
            )""".format(name)

        return sql

    # Creates the necessary tables
    def createTable(self):
        sql = "DROP TABLE IF EXISTS today"
        self.db.cursor.execute(sql)
        sql = "DROP TABLE IF EXISTS yesterday"
        self.db.cursor.execute(sql)
        sql = "DROP TABLE IF EXISTS day_before"
        self.db.cursor.execute(sql)

        sql = self.get_create_table_query("today")
        self.db.cursor.execute(sql)
        sql = self.get_create_table_query("yesterday")
        self.db.cursor.execute(sql)
        sql = self.get_create_table_query("day_before")
        self.db.cursor.execute(sql)

        self.db.connection.commit()

    # sends the json data to the sql database
    def jsonToSQL(self, fileName):
        with open(fileName, "r") as f:
            data = json.load(f)

        sql = "INSERT INTO today VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
        self.db.cursor.executemany(sql, data[0])

        sql = "INSERT INTO yesterday VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
        self.db.cursor.executemany(sql, data[1])

        sql = "INSERT INTO day_before VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
        self.db.cursor.executemany(sql, data[2])

        self.db.connection.commit()
