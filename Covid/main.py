from DBArchiver import DBArchiver
from DBConnection import DBConnection
from DataAnalysis import DataAnalysis
from Webscrap import WebScrapping
from plots import Plots

db = DBConnection("localhost", "Kausar", "Km313162", "covid", "mysql_native_password")
# db = DBConnection("localhost", "root", "E1qat2wsi3ed", "covid", "mysql_native_password")

# Scrape the data from the website
webData = WebScrapping("https://www.worldometers.info/coronavirus/#countries")
webData.data()

# Send the data to the database
archiver = DBArchiver(db)
archiver.createTable()
archiver.jsonToSQL("ext/country_neighbour_dist_file.json")

# Analyze the data
analysis = DataAnalysis(db)
analysis.analyze()

# Make graphs for the data
bar1 = Plots(db)
bar1.make_deathtoll_bar()

bar2 = Plots(db)
bar2.make_test_bar()

bar3 = Plots(db)
bar3.make_recovered_bar()
