# -*- coding: utf-8 -*-
"""
Created on Wed Mar 17 17:34:39 2021

@author: mussa
"""
from DBConnection import DBConnection
import pandas as pd

#This is a class to get the required plots for the data wanted
#There is a make and get for the 3 desired plots. The make for each plot
#stores data for each country by calling the get helper methods for each country

class Plots: 
    def __init__(self, db):
        self.db = db
    
    #This is a helper method to append all the 5 lists to 1 to be able to create the plot
    def make_df(self, first, second, third, fourth, fifth):
        all_tolls =[]
        all_tolls = first.append(second)
        all_tolls = all_tolls.append(third)
        all_tolls = all_tolls.append(fourth)
        all_tolls = all_tolls.append(fifth)
        return all_tolls

    #the method to retrive data for the death Toll for the requested country.
    #When calling the method the user must input the country whose data he desires
    def get_deathtoll_data (self, country):
        sql = "SELECT country, deaths_per_million FROM today WHERE country = %s"
        self.db.cursor.execute(sql, (country,))
        toll = self.db.cursor.fetchall()
        data = pd.DataFrame(data=toll, index = ["Today"], columns=["Country", "Death toll per 1M"])
        return data
     
    #the method will call the get death toll method to get the data
    # then will store the data in a df and make a plot bar
    def make_deathtoll_bar(self):
        first_toll = self.get_deathtoll_data ('USA')
        second_toll = self.get_deathtoll_data ('Spain')
        third_toll = self.get_deathtoll_data ('Argentina')
        fourth_toll = self.get_deathtoll_data ('Italy')
        fifth_toll = self.get_deathtoll_data ('Colombia')
        
        df = self.make_df( first_toll,second_toll, third_toll ,fourth_toll, fifth_toll)
        dframe = pd.DataFrame(data=df,  columns=["Country", "Death toll per 1M"])
        dframe.plot.bar(x ='Country', y= 'Death toll per 1M', title = 'TodayDeathsPerMillion', color = 'r')
        
    #the method to retrive data for the total tested for the requested country.
    #When calling the method the user must input the country whose data he desires
    def get_test_data (self, country):
        sql = "SELECT country, total_tests FROM today WHERE country = %s"
        self.db.cursor.execute(sql, (country,))
        toll = self.db.cursor.fetchall()
        data = pd.DataFrame(data=toll, index = ["Today"], columns=["Country", "Total Tested"])
        return data
    
        #the method will call the get test data method to get the data
        # then will store the data in a df and make a plot bar
    def make_test_bar(self):
        first_total = self.get_test_data ('Israel')
        second_total = self.get_test_data ('Saudi Arabia')
        third_total = self.get_test_data ('Pakistan')
        fourth_total = self.get_test_data ('Philippines')
        fifth_total = self.get_test_data ('Jordan')
    
        df = self.make_df( first_total,second_total, third_total ,fourth_total, fifth_total)
        dframe = pd.DataFrame(data=df,  columns=["Country", "Total Tested"])
        dframe.plot.bar(x ='Country', y= 'Total Tested', title = 'TOTAL TESTED', color = 'b')
    
     #the method to retrive data for the total recovered for the requested country.
    #When calling the method the user must input the country whose data he desires
    def get_recovered_data (self, country):
        sql = "SELECT country, total_recovered FROM today WHERE country = %s"
        self.db.cursor.execute(sql, (country,))
        toll = self.db.cursor.fetchall()
        data = pd.DataFrame(data=toll, index = ["Today"], columns=["Country", "Total Recovered"])
        return data    
    
        #the method will call the get recovered data method to get the data
    # then will store the data in a df and make a plot bar
    def make_recovered_bar(self):
        first_total = self.get_recovered_data ('Israel')
        second_total = self.get_recovered_data ('Saudi Arabia')
        third_total = self.get_recovered_data ('Pakistan')
        fourth_total = self.get_recovered_data ('Philippines')
        fifth_total = self.get_recovered_data ('Jordan')
    
        df = self.make_df( first_total,second_total, third_total ,fourth_total, fifth_total)
        dframe = pd.DataFrame(data=df,  columns=["Country", "Total Recovered"])
        dframe.plot.bar(x ='Country', y= 'Total Recovered', title = 'TOTAL RECOVERED', color = 'y')