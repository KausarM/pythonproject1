import mysql.connector

# Represents a connection to a mysql database
class DBConnection:
    def __init__(self, host, user, password, db, auth_plugin):
        self.connection = mysql.connector.connect(
            host=host,
            user=user,
            password=password,
            database=db,
            auth_plugin=auth_plugin
        )
        self.cursor = self.connection.cursor(buffered=True)
